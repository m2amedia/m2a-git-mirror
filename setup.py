"""Set up script for the m2a-git-mirror package."""

import sys

if sys.version_info < (3, 5):
    raise Exception("Python version must be ≥ 3.5")

import pathlib
import setuptools


PATH_ROOT = pathlib.Path(__file__).parent


def version():
    """Get the m2a-git-mirror package version.

    .. note::

        This is the cannonical package version. If needed at runtime,
        :mod:`pkg_resources` should be used.
    """
    return "0.1.0"


def long_description():
    """Load the README."""
    with (PATH_ROOT / "README.md").open() as readme:
        return readme.read()


setuptools.setup(
    name="m2a-git-mirror",
    version=version(),
    author="Oliver Ainsworth",
    author_email="oliver.ainsworth@m2amedia.tv",
    license="Proprietary",
    url="https://bitbucket.org/m2amedia/m2a-git-mirror/",
    description="Mirror Git repositories via git-daemon.",
    long_description=long_description(),
    long_description_content_type="text/markdown",
    py_modules=["m2a_git_mirror"],
    entry_points={
        "console_scripts": [
            "m2a-git-mirror=m2a_git_mirror:main",
        ],
    },
    install_requires=[
        "click >=6.7, <7.0",
        "portalocker >=1.2.1",
    ],
    extras_require={
        "development": [
            "pylint",
            "pytest",
            "pytest-cov",
        ],
    },
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Intended Audience :: Developers",
        "Intended Audience :: System Administrators",
        "License :: Other/Proprietary License",
        "Operating System :: POSIX :: Linux",
        "Programming Language :: Python :: 3 :: Only",
        "Programming Language :: Python :: 3.5",
        "Topic :: Software Development :: Version Control",
        "Topic :: Software Development :: Version Control :: Git",
    ],
)
