#!/usr/bin/env python3

"""Super simple Git mirroring utility.

This module implements a script that is suitable for use as an access
hook with git-deamon(1). If configured correctly, whenever a Git daemon
is accessed, it will invoke this script which will perform the actual
mirroring tasks before allowing Git to continue serving the request.
"""

import contextlib
import collections
import enum
import functools
import io
import logging
import os
import os.path
import pathlib
import re
import select
import shlex
import shutil
import subprocess
import sys
import types

import click
import portalocker


log = logging.getLogger(__name__)
REMOTE_PATTERNS = [re.compile(pattern) for pattern in [
    r"^git@bitbucket\.org:(?P<name>.*)\.git$"
]]


class GitMirrorError(Exception):
    """Base exception for everything."""


class RepositoryError(GitMirrorError):
    """Base exception for all repository errors."""


class RepositoryExistenceError(RepositoryError):
    """Raise when a repository exists when it shouldn't.

    Or doesn't exist when it should.
    """


class LockError(RepositoryError):
    """Raised when repository lock cannot be acquired."""


class LockTimeoutError(LockError):
    """Raised when a repository lock cannot be acquired in time."""


class CommandWrapper:
    """Helper wrapper for executing subprocesses.

    Specifically, this provides helpers to easily iterate over a
    subprocesses' output line by line. See :meth:`__iter__`.

    The wrapped command's output is also wrapped in standard logging
    facilities. When the command is invoked, a logger is created
    from the executables' basename and the PID of the running process.
    Stdout and stderr are both logged via this logger.

    Note that a wrapped command can only be invoked once. Attempts
    to invoke it multiple times will result in a :exc:`RuntimeError`.

    :param arguments: List of arguments to execute. As would be passed
        :class:`subprocess.Popen`.
    """

    class Stream(enum.Enum):
        """Identify command output streams."""

        STDOUT = 1
        STDERR = 2

    def __init__(self, arguments):
        self._arguments = arguments
        self._invoked = False

    def __str__(self):
        return " ".join(shlex.quote(argument)
                        for argument in self._arguments)

    def __del__(self):
        """Warn if command never executed."""
        if not self._invoked:
            log.warning("Command %s never invoked", self)

    def __iter__(self):
        """Iterate over the command's output line-by-line.

        Trailing whitespace in command output is stripped away before
        being returned.

        :raises RuntimeError: If the command as already been invoked.

        :returns: Iterator of each line in the wrapped command's stdout.
        """
        if self._invoked:
            raise RuntimeError("Command already invoked")
        self._invoked = True
        log.debug("Executing %s", self)
        process_name = os.path.basename(self._arguments[0])
        process = subprocess.Popen(
            self._arguments,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            universal_newlines=True,
        )
        process_log = logging.getLogger(
            "{process_name}.{process.pid}".format(**locals()))
        for stream, output in self._stream_process(process):
            output_safe = output.rstrip()
            output_yield = False
            if stream is self.Stream.STDOUT:
                process_log.info(output_safe)
                yield output_safe
            elif stream is self.Stream.STDERR:
                process_log.warning(output_safe)

    def run(self):
        """Run the command.

        This runs the command without capturing its output. Note, the
        output will still be logged however.

        :raises RuntimeError: If the command as already been invoked.
        """
        for _ in self:
            pass

    def _stream_process(self, process):
        """Wrap a process' output streams.

        This method provides a wrapper around a running subprocess'
        stdout and stderr output streams. Each line in these streams is
        returned as part of the iterator returned by this method as soon
        as they become available.

        The given process must have :data:`subprocess.PIPE`'s configured
        for its stdout and stderr streams. The iterator will continually
        yield lines written to these pipes until they are hung-up.

        As this method merges stdout and stderr output together,
        :class:`CommandWrapper.Stream` enumerations are used to identify
        the the stream that yielded a line.

        It is possible that the wrapped process finishes without
        completing a line in either of its output streams. In such
        cases, the incomplete line is yielded regardless, suffixed
        with a line terminator.

        The wrapped process is guaranteed to have terminated before
        the returned iterator completes.

        :param process: Process' whose output streams should be wrapped.
        :type process: subprocess.Popen

        :returns: Iterator of tuples containing a
            :class:`CommandWrapper.Stream` and a line of output.
        """
        buffer_stdout = io.StringIO()
        buffer_stderr = io.StringIO()
        stdout = (self.Stream.STDOUT, process.stdout, buffer_stdout)
        stderr = (self.Stream.STDERR, process.stderr, buffer_stderr)
        poller = select.poll()
        poller.register(process.stdout.fileno())
        poller.register(process.stderr.fileno())
        poller_registered = 2
        while poller_registered:
            for descriptor, event in poller.poll():
                if event & select.POLLIN:
                    stream, readable, buffer = (
                        stdout if descriptor
                        == process.stdout.fileno() else stderr)
                    buffer_cursor = buffer.tell()
                    buffer.write(readable.read())
                    buffer.seek(buffer_cursor)
                    for line in self._read_available_lines(buffer):
                        yield stream, line
                if event & select.POLLHUP:
                    poller.unregister(descriptor)
                    poller_registered -= 1
        for stream, _, buffer in (stdout, stderr):
            incomplete = buffer.read()
            if incomplete:
                yield stream, incomplete + "\n"
        process.wait()  # sanity checks
        assert not process.stdout.read()
        assert not process.stderr.read()

    def _read_available_lines(self, buffer):
        """Read all available lines from a buffer.

        This is a generator that attempts to read all available lines
        from a given string buffer. If the buffer ends in an incomplete
        line (as in, it's missing a line terminator) the buffer cursor
        position will be set to the position of the last complete line.

        Therefore, if the buffer doesn't contain *any* complete lines,
        the buffer position will be reset to its initial position. In
        such a case, the iterator returned by this function will be
        empty.

        Lines returned by this method include their line terminator.

        :param buffer: Buffer to read lines from.
        :type buffer: io.StringIO

        :returns: Iterator of lines in the buffer as strings.
        """
        buffer_cursor = buffer.tell()
        incomplete = False
        while not incomplete:
            line = buffer.readline()
            incomplete = not line.endswith("\n")
            if incomplete:
                buffer.seek(buffer_cursor)
            else:
                buffer_cursor = buffer.tell()
                yield line


@functools.total_ordering
class Repository:

    LOCK_TIME_OUT = 600

    def __init__(self, workspace, local, remote):
        self._workspace = workspace
        self._local = local
        self._remote = remote
        self._locked = False

    def __repr__(self):
        return ("<{0.__class__.__name__}"
                " {0._local!r} from {0._remote!r}>".format(self))

    def __eq__(self, other):
        return self.path == other.path

    def __lt__(self, other):
        return self.path < other.path

    @property
    def path(self):
        """Absolute path to the repository in the workspace."""
        return self._workspace.path_repository(self._local)

    @property
    def name(self):
        """Local name of the repository."""
        return self._local

    @property
    def remote(self):
        """Remote address of the repository."""
        return self._remote

    @contextlib.contextmanager
    def lock(self):
        """Lock the repository.

        This will attempt to acquire a workspace lock on the repository
        by locking a file which matches the repository's name. It will
        block until the lock can be acquired or a timeout is reached.

        :returns: Context manager around the lock.
        :rtype: ContextManager
        """
        log.debug("Locking repository %s", self)
        path = self._workspace.path_lock(self._local)
        with portalocker.Lock(str(path), timeout=self.LOCK_TIME_OUT):
            self._locked = True
            try:
                log.debug("Repository locked %s", self)
                yield
            finally:
                path.unlink()
                self._locked = False
                log.debug("Unlocking repository %s", self)
        log.debug("Repository unlocked %s", self)

    def clone(self):
        """Clone the respository into the workspace.

        The repository lock must be held.

        :raises RepositoryExistenceError: If the repository already
            exists in the workspace. Use :meth:`fetch` instead.
        :raises LockError: If the repository lock is not held.
        """
        path = self._workspace.path_repository(self._local)
        if path.is_dir():
            raise RepositoryExistenceError("Repository already exists")
        if not self._locked:
            raise LockError("Need lock to clone")
        log.info("Cloning %s into %s", self._remote, path)
        git = shutil.which("git")
        git_command = CommandWrapper([
            git, "clone", "--mirror", self._remote, str(path)])
        git_command.run()

    def fetch(self):
        """Fetch changes to repository from the remote.

        The repository lock must be held.

        :raises RepositoryExistenceError: If the repository does not
            exist in the workspace. Use :meth:`clone` instead.
        :raises LockError: if the repository lock is not held.
        """
        path = self._workspace.path_repository(self._local)
        if not path.is_dir():
            raise RepositoryExistenceError("Repository does not exist")
        if not self._locked:
            raise LockError("Need lock to fetch")
        log.info("Updating %s from %s", path, self._remote)
        git = shutil.which("git")
        git_command = CommandWrapper(  # TODO: check remote is correct
            [git, "-C", str(path), "remote", "update", "--prune"])
        git_command.run()

    def update(self):
        """Ensure repository is at the latest version.

        If the repository does not exist locally, it will be cloned.
        Alternatively, if it *does* exist already, then a simple fetch
        will be performed.
        """
        try:
            self.clone()
        except RepositoryExistenceError:
            self.fetch()

    def remove(self):
        """Remove local clone of repository.

        The repository lock must be held.
        """
        path = self._workspace.path_repository(self._local)
        log.info("Deleting %s", path)
        shutil.rmtree(str(path), ignore_errors=True)

class Workspace:
    """Directory containing local repository mirrors.

    A workspace contains three top-level items: a repository list
    (``list``), local repository clones (``repos/``) and a set of
    lock files (``locks/``).

    The repository list is a simple text file which describes what
    repositories should be mirrored into the workspace. Each line
    in the list must specify one repository through two whitespace
    separated values: the local name and remote.

    The local name controls what path the repository will be cloned
    to under the workspace's repository directory. Therefore it also
    controls the name of the repository that a client will request from
    git-daemon.

    The remote is any valid Git remote address.

    Repositories are loaded lazily whenever they are needed by the
    workspace. For example, by iterating over the workspace. The list
    is cached for the entire life-time of the :class:`Workspace`
    instance. Continuing the example, all subsequence iterations over
    the workspace will return the exact same set of repositories even
    if the underlying file-backed list has changed.

    :param root: Path to the directory to use as the workspace.
    :type root: Union[str, pathlib.Path]
    """

    #: Default path for workspaces
    DEFAULT = pathlib.Path(os.path.expanduser("~")) / ".m2a-git-mirror"

    def __init__(self, root):
        self._path = pathlib.Path(root).absolute()
        self._list = []
        self._list_loaded = False

    def __repr__(self):
        return "<{0.__class__.__name__} '{0._path}'>".format(self)

    def __iter__(self):
        """Iterate over all repositories in the workspace.

        See above (:class:`Workspace`) for notes on lazy loading and
        caching of repository lists.

        :returns: All repositories in the workspace's list.
        :rtype: Iterable[Repository]
        """
        self._list_read()
        yield from self._list

    @property
    def _path_list(self):
        """Path to workspace repository list file."""
        return self._path / "list"

    @property
    def _path_repositories(self):
        """Path to workspace repositories directory."""
        return self._path / "repos"

    @property
    def _path_locks(self):
        """Path to workspace lock files directory."""
        return self._path / "locks"

    def _list_read(self):
        """Load the repository list.

        This will attempt to read a repository list from a file
        called 'list' in the root of the workspace. The file is parsed
        line-by-line into a sequence of :class:`Repository` objects.

        Each line should contain two white space separated values:
        the local name of the repository and the remote address.

        Local names must be valid paths. However, if they contain a
        '..' or '.' they will be ignored with a warning being logged.

        The repository list is cached on the workspace. Once this has
        been invoked once, all subsequence invocations will be no-ops.
        """
        if self._list_loaded:
            return
        try:
            log.debug("Loading repository list from %s", self._path_list)
            with self._path_list.open() as file:
                for line in file:
                    skip = False
                    local, remote = line.strip().split(maxsplit=1)
                    local_path = pathlib.Path(local)
                    for part in local_path.parts:
                        if part in [".", ".."]:
                            skip = True
                            log.warning(
                                "Ignoring {}: repository names cannot"
                                " contain '.' or '..'".format(local_path))
                    if not skip:
                        repository = Repository(self, local, remote)
                        self._list.append(repository)
                        log.info("Discovered %s", repository)
        except FileNotFoundError:
            log.error("No list {}; treating as empty".format(self))
        self._list_loaded = True

    def add(self, local, remote):
        """Add a repository to the workspace.

        This will add the given repository to the workspace, adding
        it to the file-backed repository list.

        :param local: Local name of the repository.
        :type local: str
        :param remote: Address of remote Git repository.
        :type remote: str

        :raises RepositoryExistenceError: If a repository with the
            given local name already exists in the workspace.
        """
        repository_new = Repository(self, local, remote)
        for repository in self:
            if repository_new.path == repository.path:
                raise RepositoryExistenceError(
                    "Workspace already has a repository {!r}".format(local))
        log.debug("Appending repository to %s", self._path_list)
        with self._path_list.open("a") as list_:
            list_.write("{} {}\n".format(local, remote))
        self._list.append(repository_new)

    def remove(self, local):
        """Remove a repository from the workspace.

        This will remove a repository from the workspace by the given
        local repository name. The repository's local copy is deleted
        and removed from the workspace's repository list.
        """
        repositories = []
        for repository in self:
            if local == repository.name:
                repository.remove()
            else:
                repositories.append(repository)
        log.debug("Writing repository list to %s", self._path_list)
        with self._path_list.open("w") as list_:
            for repository_retained in repositories:
                list_.write("{} {}\n".format(
                    repository_retained.name, repository_retained.remote))
        self._list.clear()
        self._list.extend(repositories)

    def find(self, path):
        """Map a path onto a repository.

        This takes a path as provided by git-daemon and maps it onto
        a repository managed by the workspace.

        :param path: Path of repository to find.
        :type path: Union[str, pathlib.Path]

        :returns: Repository in the workspace if any.
        :rtype: Optional[Repository]
        """
        path = pathlib.Path(path)
        log.debug("Searching for repository with path %s", path)
        for repository in self:
            repository_path = pathlib.Path("/".join(
                repository.path.parts[-len(path.parts):]))
            if repository.path == path or repository_path == path:
                return repository

    def path_lock(self, name):
        """Calculate path to a lock file.

        :param name: Local name of the repository.
        :type name: str

        :returns: Full path to the repository's lock file.
        :rtype: pathlib.Path
        """
        path = self._path_locks / name
        path.parent.mkdir(parents=True, exist_ok=True)
        return path

    def path_repository(self, name):
        """Calculate path to repository on disk.

        :param name: Local name of the repository.
        :type name: str

        :returns: Full path to the repository.
        :rtype: pathlib.Path
        """
        path = self._path_repositories / name
        path.parent.mkdir(parents=True, exist_ok=True)
        return path

    def ensure(self):
        """Ensure the workspace exists.

        This will create the workspace directory if it doesn't exist.
        Empty sub-directories for repositories and locks files will
        also be created. An empty repository list is also added.

        If the workspace already exists with its repository list and
        all of its sub-directories then this method does nothing.

        If the workspace is in a partial state -- e.g. with one of
        the sub-directories missing -- then it will be repaired.
        """
        for path in [self._path, self._path_repositories, self._path_locks]:
            log.info("Creating %s", path)
            path.mkdir(parents=True, exist_ok=True)
        log.info("Creating %s", self._path_list)
        self._path_list.touch()

    def prune(self):
        """Delete stale repositories and locks.

        This will check the workspace list and delete any local
        repositories or locks that don't have corresponding list
        entries.

        Workspaces should be pruned whenever an item is removed from
        the mirror list.
        """
        log.info("Pruning workspace %s", self._path)


def guess_remote_name(remote):
    """Guess local repository name from remote.

    This will attempt to determine a local name for repository mirror
    based off of the repository's remote address.

    :param remote: Git address for a remote repository.
    :type remote: str

    :raises ValueError: If no name can be determined for the remote.

    :returns: Local name inferred from the remote.
    :rtype: str
    """
    name = None
    for pattern in REMOTE_PATTERNS:
        match = pattern.match(remote)
        if match:
            try:
                name = match.group("name")
            except IndexError:
                pass
    if not name:
        raise ValueError("Cannot guess name for {!r}".format(remote))
    return name


@click.group()
@click.option(
    "-p",
    "--path",
    type=pathlib.Path,
    default=Workspace.DEFAULT,
    help="Directory to use for repositories and lock files.",
)
@click.pass_context
def main(context, path):
    """Super simple Git mirroring utility.

    This utility implments two sub-commands used to mirror Git
    repositories: 'synchronise' and 'hook'. The latter is used as an
    access hook with git-daemon(1) which is used to serve the mirrors
    to clients.

    All sub-commands operate on a workspace (specified via --path)
    which is a directory containing the local copies of the repository
    clones. This workspace also contains a 'list' file which enumerates
    the repositories to clone.

    The repository list format is a whitespace separated list of
    local names and Git remotes. The remote is the repository to be
    mirrored. The local name is path the repository will be exposed
    as via git-daemon. For example:

        repository-mirror git@bitbucket.org:user/repository.git
        foo/other-mirror git@bitbucket.org:user/other.git

    With each change to the repository list, the 'synchronise' command
    must be invoked to perform the initial clones. This will also clean
    up any old local copies that are no longer needed.

    The access hook will ensure that only the latest versions of a
    repository is served to a client. Periodically re-synchronising
    can reduce the ammount of work the access hook has to do to keep
    repositories up to date.

    Mirrored repositories are placed into the workspace under the
    repos/ sub-directory. For example:

        $WORKSPACE/
            list
            locks/
            repos/
                repository-mirror/
                    ...
                foo/
                    other-mirror/
                        ...

    The default workspace location is ~/.m2a-git-mirror/.
    """
    logging.basicConfig(
        level=logging.DEBUG,
        format="%(asctime)s %(levelname)s %(message)s",
        stream=sys.stderr,
    )
    context.obj = Workspace(path)


@main.command("initialise")
@click.pass_context
def _main_initialise(context):
    """Initialise a workspace.

    This will create the necessary directory structure in the given
    workspace, making it suitable for subsequent m2a-git-mirror calls.
    """
    workspace = context.obj
    log.info("Initialising workspace %s", workspace)
    workspace.ensure()


@main.command("list")
@click.option(
    "-t",
    "--template",
    type=str,
    default="{repository.local.name}",
)
@click.pass_context
def _main_list(context, template):
    """List all mirrored repositories.

    This will print a line to stdout for each repository that has
    been added to the workspace. By default, just the local name of
    the repository is printed. The format of the lines can be controlled
    through the --template option.

    Templates may be regular Python format string templates, with the
    following values available:

        repository.local.name
        repository.local.path
        repository.remote

    Results are always alphabetically sorted by repository name.
    """
    workspace = context.obj
    for repository in sorted(workspace):
        namespace = types.SimpleNamespace()
        namespace.local = types.SimpleNamespace()
        namespace.local.name = repository.name
        namespace.local.path = repository.path
        namespace.remote = repository.remote
        print(template.format(repository, repository=namespace))


@main.command("add")
@click.argument("remote")
@click.argument("name", required=False)
@click.option(
    "-f",
    "--force",
    is_flag=True,
    help="Overwrite repository with same name if it already exists.",
)
@click.pass_context
def _main_add(context, remote, name, force):
    """Add a repository to be mirrored.

    This will add the given remote to the workspace's repository list.
    An attempt is made to infer the local name of the repository from
    the remote if not explicitly given.

    The repository name must be unique for the workspace.

    Note that by default this only adds the remote to the list of
    repositories. To fetch it from the remote use 'synchronise'.

    By default, this return with an error if a repository with the
    same name already exists in the workspace. However, if --force
    is given, then the previous repository with the same name is
    removed (if necessary) before adding the new one.
    """
    workspace = context.obj
    if not name:
        try:
            name = guess_remote_name(remote)
        except ValueError as error:
            log.error(str(error))
            sys.exit(1)
    added = False
    log.info("Adding %s as %s", remote, name)
    while not added:
        try:
            workspace.add(name, remote)
            added = True
        except RepositoryExistenceError as error:
            if force:
                prior = workspace.find(name)
                if prior.remote == remote:
                    log.info(
                        "Repository %s already added; doing nothing", name)
                    added = True
                else:
                    log.info("Repository %s already added"
                             " with different remote; removing", name)
                    workspace.remove(name)
            else:
                log.error(str(error))
                sys.exit(1)


@main.command("synchronise")
@click.argument("repository", nargs=-1)
@click.pass_context
def _main_synchronise(context, repository):
    """Syncrhonise repositories to their remotes.

    This will update all repositories in the given workspace's
    repository list, cloning them if necessary. Any artifacts for
    repositories that are no longer in the list are also removed. Hence,
    this should be run whenever the repository list changes.

    Synchronisation can be restricted to specific repositories by
    providing one or more local names.
    """
    workspace = context.obj
    repositories = workspace
    if repository:
        repositories = []
        for repository_pending in repository:
            repositories.append(workspace.find(repository_pending))
    log.info("Synchronising workspace %s", workspace)
    for repository_stale in repositories:
        with repository_stale.lock():
            repository_stale.update()
    workspace.prune()


@main.command("hook")
@click.argument("service")
@click.argument("path", type=pathlib.Path)
@click.argument("hostname")
@click.argument("hostname-canonical")
@click.argument("ip")
@click.argument("port")
@click.pass_context
def _main_hook(context, service,
                 path, hostname, hostname_canonical, ip, port):
    """Git daemon access hook.

    This sub-command implements an access hook that is suitable for
    use with git-daemon(1). See git-daemon documentation for details
    on how this hook is invoked.

    Note, Git will set the working directory to the Git repository
    that was requested before invoking the access hook. Therefore, make
    sure that the given --workspace is an absolute path.

    Example, basic git-daemon usage:

        WORKSPACE=/var/git/
        git daemon \
            --export-all \
            --base-path=$WORKSPACE/repos \
            --access-hook="m2a-git-mirror --workspace $WORKSPACE/ hook"

    With the above usage, whenever a Git client tries to read from the
    Git daemon, the access hook is invoked. The hook will map the
    requested repository path onto one in the given workspace.

    Before the hook allows the response to be sent to the client,
    it will update the Git repository from the remote specified by
    the workspace repository list.

    Also note that Git will reject a request before invoking the hook
    if it cannot locate a Git repository in the --base-path. Ensuring
    the workspace is synchronised after each change to the repository
    list will avoid this.
    """
    workspace = context.obj
    if service != "upload-pack":
        raise Exception("Mirrored repositories are read-only")
    repository = workspace.find(path)
    if repository is None:
        print("Unknown repository {}".format(path))
        sys.exit(1)
    with repository.lock():
        repository.fetch()


if __name__ == "__main__":
    main()
